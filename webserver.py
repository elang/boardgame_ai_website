import os

from bottle import *
from docker import Client
from io import BytesIO
from pymongo import MongoClient
import pymongo
from bson.objectid import ObjectId
import sys
from datetime import datetime
from bots import *
from eloCalculator import EloCalculation
from beloRater import BeloCalculator


def get_game_record(game, limit=1000):
    db = MongoClient()[game]
    return db.games.find().sort("date", pymongo.DESCENDING)[:limit]

def make_result_table(results):
    return '\n'.join(['<tr><td>{}</td><td>{}</td><td>{}</td><td><a href="/see_game/{}">Show Game</a></td></tr>'.format(result["northPlayerName"], result["southPlayerName"], result["winner"], result["_id"])  for result in results])

@get("/lastResults")
def get_last_results():
    results = get_game_record("battleline")
    return '<html><body><a href="/winLoss">Win Loss Table</a><br /><table><tr><th>North</th><th>South</th><th>winner</th><th></th></tr>' + make_result_table(results) +"</table></body></html>"

@get("/api/<game>/lastResults")
def get_last_results(game):
    limit = request.json["limit"] if limit in request.json else 1000
    return get_game_record(game, limit)

def get_stat_block(game, date="2016-01-01"):
    db = MongoClient()[game]
    dateobj = datetime.strptime(date, '%Y-%m-%d')
    stat_block = []
    for player,bot in battleline_bots.items():
        north_total = db.games.count({"northPlayerName" : bot["name"], "date" : {"$gte" : dateobj}})
        south_total = db.games.count({"southPlayerName" : bot["name"], "date" : {"$gte" : dateobj} })
        north_wins = db.games.count({"northPlayerName" : bot["name"], "winner": "{} wins".format(bot["name"]), "date" : {"$gte" : dateobj} })
        south_wins = db.games.count({"southPlayerName" : bot["name"], "winner": "{} wins".format(bot["name"]), "date" : {"$gte" : dateobj} })
        north_losses = north_total-north_wins
        south_losses = south_total-south_wins
        total = north_total + south_total
        total_wins = north_wins + south_wins
        total_losses = north_losses + south_losses
        north_pct = float(north_wins)/north_total if north_total else 0
        south_pct = float(south_wins)/south_total if south_total else 0
        total_pct = float(total_wins)/total if total else 0
        eloEntry = db.players.find_one({"bot_name" : bot["name"]})
        if eloEntry:
            elo = int(eloEntry[game])
            belo = int(eloEntry[game+"_belo"])
            version = eloEntry["version"]
        else:
            elo = 0
            belo = 0
            version = 1


        stat_block.append((bot["name"], player, version, north_wins, north_losses, north_pct, south_wins, south_losses, south_pct,  total_wins, total_losses, total_pct, elo, belo))

    stat_block.sort(key=lambda x: x[-2], reverse=True)
    return stat_block



def get_stat_table_html(date="2016-01-01"):
    table_headers = ["Bot", "Player", "North Wins", "North Losses", "North %", "South Wins", "South Losses", "South %", "Total Wins", "Total Losses", "Total Win %", "elo", "belo"]
    results = '<table border="1"><tr>{}</tr>'.format(''.join(["<th>{}</th>".format(header) for header in table_headers]))
    stat_block = get_stat_block("battleline", date)
    for items in stat_block:
        results += "<tr>" + ''.join(["<td>{}</td>".format(item) for item in items]) + "</tr>"
    return results + "</table>"
    
@get("/winLoss")
def get_win_loss_record():
    return get_stat_table_html()

@get("/winLoss/<date>")
def get_win_loss_record(date):
    return get_stat_table_html(date)

@get("/api/<game>/winLoss")
def get_win_loss_info(game):
    date = request.json["date"] if "date" in json else "2016-01-01"
    return get_stat_block("battleline", game)

def get_head_to_head_info(game, bot1, bot2, date):
    db = MongoClient()[game]
    player1 = battleline_bots[bot1]["name"]
    player2 = battleline_bots[bot2]["name"]
    dateobj = datetime.strptime(date, '%Y-%m-%d')
    
    north_wins = db.games.count({"northPlayerName" : player1, "southPlayerName": player2, "winner": "{} wins".format(player1), "date" : {"$gte" : dateobj} })
    north_losses = db.games.count({"northPlayerName" : player1, "southPlayerName": player2, "winner": "{} wins".format(player2), "date" : {"$gte" : dateobj}})
    
    south_wins = db.games.count({"northPlayerName" : player2, "southPlayerName": player1, "winner": "{} wins".format(player1), "date" : {"$gte" : dateobj} })
    south_losses = db.games.count({"northPlayerName" : player2, "southPlayerName": player1, "winner": "{} wins".format(player2), "date" : {"$gte" : dateobj}})
    
    total_wins = north_wins + south_wins
    total_losses = north_losses + south_losses
    return {"north_wins": north_wins, "north_losses": north_losses, "south_wins": south_wins, "south_losses" : south_losses}
    
def get_head_to_head_results(bot1, bot2, date="2016-01-01"):
   
    obj = get_head_to_head_info("battleline", bot1, bot2, date)
    north_wins = obj["north_wins"] 
    north_losses = obj["north_losses"]
    south_wins = obj["south_wins"]
    south_losses = obj["south_losses"]
    total_wins = north_wins + south_wins
    total_losses = north_losses + south_losses
    table_header = "<tr><th>North Record</th> <th>South Record</th> <th>Total Record</th></tr>"
    table_info = "<tr><th>{}-{}</th> <th>{}-{}</th> <th>{}-{}</th></tr>".format(north_wins, north_losses, south_wins, south_losses, total_wins, total_losses)
    return '{}\'s record against {} <table border="1">{}{}</table>'.format(bot1, bot2, table_header, table_info)

        
@get("/headToHead/<bot1>/<bot2>")
def seeHeadToHead(bot1, bot2):
    return get_head_to_head_results(bot1, bot2)

@get("/headToHead/<bot1>/<bot2>/<date>")
def seeHeadToHead(bot1, bot2, date):
    return get_head_to_head_results(bot1, bot2, date)

@get("/api/<game>/headToHead")
def get_head_to_head_results_json():
    if "north_Bot" not in request.json and "south_bot" not in request.json:
        abort(400)
    else:
        date = request.json["date"] if date in request.json else "2016-01-01"
        return get_head_to_head_info("battleline", request.json["north_bot"], request.json["south_bot"], date)

def get_game_information(game, id):
  db = MongoClient()[game]
  obj = db.games.find_one({"_id": ObjectId(id)})
  return obj
    
@get("/api/<game>/see_game")
def see_game(game):
    if "id" not in request.json:
        abort(400)
    else:
        return get_game_information(game, request.json["id"])

@get("/see_game/<id>")
def see_game(id):
  obj = get_game_information("battleline", id)
  steps_taken = obj["actionsTaken"]
  str = "{} is north<br />{} is south<br />".format(obj["northPlayerName"], obj["southPlayerName"])
  str +=  "<br />".join(steps_taken)
  return str + "<br />{}".format(obj["winner"])

@get("/")
def index():
    return template("templates/index", stat_block = get_stat_block("battleline"), bots=battleline_bots)

def get_winner_name(docker_log_string):
    winnerNameRegex = re.compile(".*(north|south)\s+\-\>\s+(.*)\s+HAS\s+WON",re.I | re.S)
    return winnerNameRegex.match(docker_log_string)

def get_player_entry(game, playersDB, bot):
    playerEntry = playersDB.find_one({"bot_name" : battleline_bots[bot]["name"]})
    if not playerEntry:
        initPost = {"bot_name": battleline_bots[bot]["name"],
                        game: 1000,
                        game + "_belo": 1000,
                        "version": 1,
                        "games_played": 0}
        post_id = playersDB.insert_one(initPost).inserted_id
        playerEntry = playersDB.find_one({"bot_name" : battleline_bots[bot]["name"]})

    return playerEntry

def get_new_elo_scores(game, playersDB, bot1, bot2, winnerName):
    bot_one_entry = get_player_entry(game,playersDB,bot1)
    bot_two_entry = get_player_entry(game,playersDB,bot2)
    bot_one_elo = int(bot_one_entry[game])
    bot_two_elo = int(bot_two_entry[game])

    number_of_players = playersDB.count({})
    db = MongoClient()[game]
    sides = ["north", "south"]
    number_of_games_per_player_in_a_tournament = (number_of_players -1) * len(sides)
    bot1_games = bot_one_entry['games_played']
    bot2_games = bot_two_entry['games_played']

    number_of_tournaments_bot1 = (bot1_games) / number_of_games_per_player_in_a_tournament
    number_of_tournaments_bot2 = (bot2_games) / number_of_games_per_player_in_a_tournament
    tournament_index = 1 + min(number_of_tournaments_bot1, number_of_tournaments_bot2)


    aging_k_value = max( 200/tournament_index, 16)

    eloCalc = EloCalculation(aging_k_value)
    scores = [1,0]
    if winnerName.group(2) == battleline_bots[bot1]["name"]:
        elos = [bot_one_elo,bot_two_elo]
    elif winnerName.group(2) == battleline_bots[bot2]["name"]:
        elos = [bot_two_elo,bot_one_elo]
    else:
        elos = [bot_one_elo,bot_two_elo]
        scores = [1,1]

    newElos = eloCalc.updateEloRatings(elos,scores)
    if winnerName.group(2) == battleline_bots[bot1]["name"]:
        bot_one_elo = newElos[0]
        bot_two_elo = newElos[1]
    elif winnerName.group(2) == battleline_bots[bot2]["name"]:
        bot_one_elo = newElos[1]
        bot_two_elo = newElos[0]
    return bot_one_elo,bot_two_elo

def get_new_belo_scores(game, playersDB, bot1, bot2, winnerName):
    bot_one_entry = get_player_entry(game,playersDB,bot1)
    bot_two_entry = get_player_entry(game,playersDB,bot2)
    bot_one_belo = int(bot_one_entry[game+"_belo"])
    bot_two_belo = int(bot_two_entry[game+"_belo"])
    beloCalc = BeloCalculator()
    if winnerName.group(2) == battleline_bots[bot1]["name"]:
        belos = [bot_one_belo,bot_two_belo]
    elif winnerName.group(2) == battleline_bots[bot2]["name"]:
        belos = [bot_two_belo,bot_one_belo]
    else:
        return bot_one_belo,bot_two_belo

    new_belos = beloCalc.calculate_new_ratings(belos)
    if winnerName.group(2) == battleline_bots[bot1]["name"]:
        bot_one_belo = new_belos[0]
        bot_two_belo = new_belos[1]
    elif winnerName.group(2) == battleline_bots[bot2]["name"]:
        bot_one_belo = new_belos[1]
        bot_two_belo = new_belos[0]
    return bot_one_belo,bot_two_belo
def get_version(bot):
    try:
        with open('{}/executables/{}/version'.format(executables_path, battleline_bots[bot]["folder"]),'r') as f:
            bot_version = f.read()
    except IOError:
        bot_version = 1
    return bot_version

def run_game(game, bot1, bot2):
    cli = Client(base_url='unix://var/run/docker.sock')
    cli.build("docker", game)
    ctr = cli.create_container(game, volumes=['/home/battleline/player1', '/home/battleline/player2'],
                    host_config=cli.create_host_config(binds=[
                        '{}/executables/{}:/home/battleline/player1'.format(executables_path, battleline_bots[bot1]["folder"]),
                        '{}/executables/{}:/home/battleline/player2'.format(executables_path, battleline_bots[bot2]["folder"]),
                    ],
                    network_mode="host"))
    cli.start(ctr)
    cli.stop(ctr, 300)
    text = cli.logs(ctr)

    winnerName = get_winner_name(text)
    if winnerName:
        db = MongoClient()[game]
        if not "players" in db.collection_names(include_system_collections=False):
            db.create_collection("players")
        playersDB = db.players
        bot_one_elo, bot_two_elo  = get_new_elo_scores( game, playersDB, bot1, bot2, winnerName)
        bot_one_belo,bot_two_belo = get_new_belo_scores(game, playersDB, bot1, bot2, winnerName)
        
        bot_one_games_played = get_player_entry(game, playersDB, bot1)["games_played"]
        bot_two_games_played = get_player_entry(game, playersDB, bot2)["games_played"]

        bot_one_version = get_version(bot1)
        bot_two_version = get_version(bot2)

        if bot_one_version != get_player_entry(game,playersDB,bot1)['version']:
            bot_one_games_played = 0
        if bot_two_version != get_player_entry(game,playersDB,bot2)['version']:
            bot_two_games_played = 0


        playersDB.update({"bot_name": battleline_bots[bot1]["name"]}, {
                          '$set': {game: bot_one_elo,
                                    game + "_belo" : bot_one_belo,
                                    "version": bot_one_version,
                                    "games_played": bot_one_games_played+1}})
        playersDB.update({"bot_name": battleline_bots[bot2]["name"]}, {
                          '$set': {game: bot_two_elo,
                                    game + "_belo" : bot_two_belo,
                                    "version": bot_two_version,
                                    "games_played": bot_two_games_played+1}})

    cli.remove_container(ctr)
    return text


@post("/api/<game>/run")
def run_server(game):
    if "north_bot" not in request.json and "south_bot" not in request.json:
        abort(400)
    else:
        return run_game(game, request.json["north_bot"], request.json["south_bot"])
    
@get("/runServer/<bot1>/<bot2>")
def run_server(bot1, bot2):
    return run_game("battleline", bot1, bot2)
    
@route("/<file:path>")
def static_route(file):
    return static_file(file, ".")

executables_path = "/home/boardgame" if len(sys.argv) < 2 else sys.argv[1]
run(host="0.0.0.0", port=8001, server="cherrypy")


