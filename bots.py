battleline_bots = { 
        "bruce": {"name": "SynergyBot", "folder": "battlelinebot", "repo": ""},
        "pat": {"name": "ElixirBot", "folder": "battleline-ai-bot", "repo": "https://github.com/pviafore/battleline-ai-bot"},
        "don": {"name": "rusty_battleline_bot", "folder": "rusty_battleline_bot", "repo": ""},
        "jonathanD": {"name": "Drofbot", "folder": "densford", "repo": ""},
        "jonathanH": {"name": "darthbagel", "folder": "battlebot", "repo": "https://github.com/jonathanhood/battlebot"},
        "andrew": {"name": "random_starterbot", "folder": "totally_python", "repo": ""},
        "may": {"name": "may_starterbot", "folder":"china_doll", "repo": ""},
        "eric": {"name": "RandoBot", "folder":"randobot", "repo": "https://github.com/ealang/battlelinebot"} 
       }

