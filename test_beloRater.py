from beloRater import BeloCalculator

def test_equalPreviousScoresTwoPlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000])
    assert returnedElos[0] == 1016
    assert returnedElos[1] == 984

def test_unequalPreviousScoresTwoPlayer400diff():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1400,1000])
    assert returnedElos[0] == 1400
    assert returnedElos[1] == 1000

def test_unequalPreviousScoresTwoPlayer400diffUnexpectedWin():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1400])
    assert returnedElos[0] == 1032
    assert returnedElos[1] == 1368

def test_unequalPreviousScoresTwoPlayer200diffUnexpectedWin():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1200])
    assert returnedElos[0] == 1018
    assert returnedElos[1] == 1182

def test_unequalPreviousScoresTwoPlayer200diffExpectedWin():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1200,1000])
    assert returnedElos[0] == 1215
    assert returnedElos[1] == 985

def test_equalPreviousScoresThreePlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000,1000])
    assert returnedElos[0] == 1016
    assert returnedElos[1] == 1000
    assert returnedElos[2] == 984

def test_unequalPreviousScoresThreePlayerUnexpected():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([800, 1200, 1000])
    assert returnedElos[0] == 830
    assert returnedElos[1] == 1182
    assert returnedElos[2] == 988

def test_unequalPreviousScoresThreePlayerUnexpectedOneReallyLow():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([800, 1400, 100])
    assert returnedElos[0] == 818
    assert returnedElos[1] == 1382
    assert returnedElos[2] == 100

def test_equalPreviousScoresFourPlayer():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([1000,1000,1000,1000])
    assert returnedElos[0] == 1014
    assert returnedElos[1] == 1003
    assert returnedElos[2] == 1000
    assert returnedElos[3] == 983

def test_unequalPreviousScoresThreePlayerUnexpectedOneReallyLow():
    myRater = BeloCalculator()
    returnedElos = myRater.calculate_new_ratings([100, 800, 1400])
    assert returnedElos[0] == 130
    assert returnedElos[1] == 802
    assert returnedElos[2] == 1368