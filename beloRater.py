class BeloCalculator:
    def __init__(self, k=32, pos_range=400):
        self.K = k
        self.range = pos_range

    def calculate_new_ratings(self, belo_scores_in_order_of_current_game_results):
        new_score_list = []
        number_of_players_minus_one = len(belo_scores_in_order_of_current_game_results) - 1
        for i,score in enumerate(belo_scores_in_order_of_current_game_results):
            game_score = 1.0 - (i*(1.0/number_of_players_minus_one))
            total_belo_scores = sum(belo_scores_in_order_of_current_game_results)
            score_diff = (score*len(belo_scores_in_order_of_current_game_results)) - total_belo_scores
            if score_diff >= self.range:
                expected_score = 1
            elif score_diff <= (self.range*-1):
                expected_score = 0
            else:
                expected_score = float(score)/float(total_belo_scores)
            new_score_list.append(self.get_new_belo(score, game_score, expected_score))
        new_score_list = self.balance_belo_differences(belo_scores_in_order_of_current_game_results, new_score_list)
        return new_score_list

    def get_new_belo(self,previous_belo, actual_score, expected_score):
        return int(previous_belo + self.K*(actual_score-expected_score))

    def balance_belo_differences(self, belo_scores_in_order_of_current_game_results, new_score_list):
        diff_belo_sum = sum(new_score_list) - sum (belo_scores_in_order_of_current_game_results)
        if diff_belo_sum == 0:
            return new_score_list
        else:
            for i,score in enumerate(belo_scores_in_order_of_current_game_results):
                diff = new_score_list[i] - score
                if abs(diff) > 0 and diff_belo_sum != 0:
                    if (diff_belo_sum) > 0 and (diff > (self.K*-1)) :
                        new_score_list[i] -= 1
                    elif diff < self.K:
                        new_score_list[i] += 1
                diff_belo_sum = sum(new_score_list) - sum (belo_scores_in_order_of_current_game_results)
            return self.balance_belo_differences(belo_scores_in_order_of_current_game_results, new_score_list)
        